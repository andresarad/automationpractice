import { $ } from '@wdio/globals'

export class ShoppingCartPage {

    public get quantatyConfirm() {
        return $('input.cart_quantity_input.form-control.grey')
    }

    public get totalProduct() {
        return $('#total_product')
    }

    public get totalShipping() {
        return $('#total_shipping')
    }

    public get totalPrice() {
        return $('#total_price')
    }
    
    public get proccedCheckoutShopping(){
        return $('.button.btn.btn-default.standard-checkout.button-medium')
    }

    public get addressInfo(){
        return $('#uniform-id_address_delivery')
    }

    public get commentArea(){
        return $('#ordermsg .form-control')
    }

    public get proccedCheckout(){
        return $('.cart_navigation button.btn-default.button-medium')
    }

    public get deliveryPrice(){
        return $('.delivery_option_price')
    }

    public get serviceCheckbox() {
        return $('#uniform-cgv')
        // return $('#cgv')
    }

    public get payByCheck(){
        return $('.cheque')
    }

    public get payByBankwire() {
        return $('.bankwire')
    }

    public get amount() {
        return $('#amount')
    }

    public get confirmationOrder(){
        return $('.alert.alert-success') 
    }

    public get viewOrderHistory(){
        return $('.button-exclusive.btn.btn-default')
    }

    public get confirmDate(){
        return $('tbody .first_item .history_date')
    }

    public get cartEmpty(){
        return $('.alert.alert-warning')
    }

    public get msgErrorBox(){
        return $('.fancybox-error')
    }
  
}

export default new ShoppingCartPage();
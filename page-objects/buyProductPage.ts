import { $ } from '@wdio/globals'

const product = process.env.PRODUCT || '';
const product2 = process.env.PRODUCT2 || '';

export class BuyProductPage {

    public get categoryWomen() {
        return $('a.sf-with-ul[title="Women"]')
    }

    public get selectColor() {
        return $('#layered_id_attribute_group_14') //Color azul
    }
    public get selectColorWhite() {
        return $('#layered_id_attribute_group_') //Color blanco
    }

    public get productName() {
        return $(`.product-container h5 .product-name[title="${product}"]`)
    }
    
    public get productName2() {
        return $(`.product-container h5 .product-name[title="${product2}"]`)
    }
    
    public get colorBlue() {
        return $('#color_14')
    }

    public get availableValue(){
        return $('#availability_value')
    }

    public get quantityWanted() {
        return $('#quantity_wanted')
    }

    public get selectSize() {
        return $('#group_1')
    }
   
    public get btnAddtoCart() {
        return $('button.exclusive')
    }
    
    public get succesAddShoping() {
        return $('.icon-check')
    }

    public get proccedCheckout(){
        return $('.btn.btn-default.button.button-medium')
        
    }

    public get shoppingCart(){
        return $('.shopping_cart a b')
    }
    
    public get purchaseSearch(){
        return $('#search_query_top')
    }
    public get btnSearch(){
        return $('.btn-default.button-search')
    }
    
    public get colorWhite() {
        return $('#color_8')
    }
}

export default new BuyProductPage();
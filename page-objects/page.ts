import { browser } from '@wdio/globals'

export default class Page {
    public open () {
        browser.url(`http://www.automationpractice.pl/index.php`)
        browser.maximizeWindow()
    }
}
import { $ } from '@wdio/globals'
import Page from './page';

export class SigninPage extends Page {

    public get signinHeader() {
        return $('.login')
    }

    public get emailInput() {
        return $('#email')
    }

    public get passwordInput() {
        return $('#passwd')
    }

    public get submitSigninBtn() {
        return $('#SubmitLogin')
    }

    public get helloUserTitle() {
        return $('.account span')
    }

    public get logout(){
        return $('.logout')
    }

    public open () {
        return super.open();
    }

}

export default new SigninPage();
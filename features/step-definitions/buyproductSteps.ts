import { After, Given, When, Then , Before } from '@wdio/cucumber-framework';
import SigninPage  from '../../page-objects/signinPage';

import { SigninTask } from '../tasks/signinTask';
import { BuyProductTask } from '../tasks/buyProductTask';
import { ShoppingCartTask } from '../tasks/shoppingCartTask';

import { expect } from 'chai';

const email = process.env.EMAIL || '';
const password = process.env.PASSWORD || '';
const comment = process.env.COMMENT || '';
const msg = process.env.CONFIRMATION || '';
const productSOut = process.env.PRODUCTSOLDOUT || '';
const emptycart = process.env.EMPTYCART || '';
const product = process.env.PRODUCT2 ||'';
const boxMsgError = process.env.MSGERROR ||'';


const signinTask = new SigninTask()
const buyProductTask = new BuyProductTask()
const shoppingCartTask = new ShoppingCartTask()

Before(async () => {
    await SigninPage.open();
    await signinTask.clickSigninHeader();
    await signinTask.automationpractice(email, password);
});


Given(/^The registered user opens the Automation Practice page$/, async () => {

})

When(/^The user chooses the color and selects a short$/, async () => {
    await buyProductTask.goToWomenShop();
    await buyProductTask.selectProduct();
    await buyProductTask.proccedCheck();
})

When(/^The user proceeds to checkout and makes the purchase$/, async () => {
    await shoppingCartTask.buyProduct(comment)
})

Then(/^The user verifies that the order has been placed correctly$/, async () => {
    const text =  await shoppingCartTask.confirmationPurchase()
    await expect(text).to.equal(msg)
    await signinTask.clickLogout()
      
})


Given(/^The registered user is on the page and wants to make a purchase of an out of stock product$/, async () => {

})

When(/^The user selects a product and tries to add the product to the cart$/, async () => {
    await buyProductTask.goToWomenShop();
    await buyProductTask.SelectProductSoldOut();
})

Then(/^A message is expected to be displayed indicating that the product is out of stock and not to be added to the cart$/, async () => {
    const textAvailable =  await buyProductTask.productAvailable()
    await expect(textAvailable).to.equal(productSOut)
    await buyProductTask.goToCart()
    const textEmpty = await shoppingCartTask.scartEmpty()
    await expect(textEmpty).to.equal(emptycart)    
    await signinTask.clickLogout()
})

Given(/^The registered user is on the home page of the website$/, async () => {

})

When(/^The user enters a search term in the search field and presses enter or clicks the search button$/, async () => {
    await buyProductTask.searchByName(product)
})
 

When(/^The user selects the resulting product and adds it to the cart$/, async () => {
    await buyProductTask.selectProductWhite();
    await buyProductTask.proccedCheck();
})

When(/^The user selects several products from different categories and adds them to the cart$/, async () => {
    await buyProductTask.goToWomenShop();
    await buyProductTask.selectProduct();
    await buyProductTask.searchByName(product)
    await buyProductTask.selectProductWhite();{}
    await buyProductTask.proccedCheck();
  
})

Given(/^The registered user has added at least one product to the shopping cart$/, async () => {
    await buyProductTask.goToWomenShop();
    await buyProductTask.selectProduct();
    await buyProductTask.proccedCheck();
})

When(/^The user goes to the checkout process and does not accept the terms of service$/, async () => {
    await shoppingCartTask.buyProductBox(comment)
})

Then(/^An error message is expected to be displayed$/, async () => {
    const msgError = await shoppingCartTask.boxmsgError()
    await expect(msgError).to.equal(boxMsgError)
    await signinTask.clickLogout()
})


 
// After(async () => {
//     await signinTask.clickLogout()
// });








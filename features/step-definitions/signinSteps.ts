import { Given, When, Then } from '@wdio/cucumber-framework';
import SigninPage from '../../page-objects/signinPage';
import { SigninTask } from '../tasks/signinTask';

import {expect} from 'chai';

const email = process.env.EMAIL || '';
const password = process.env.PASSWORD || '';
const user = process.env.USER || ''

const signinTask = new SigninTask()

Given(/^The user enters the Automation Practice homepage$/, async () => {
    await SigninPage.open()
})

When(/^The user enters the correct email and password$/, async () => {
    await signinTask.clickSigninHeader();
    await signinTask.automationpractice(email, password);
})


Then(/^The user sees Sign Out link on Account & Lists$/, async () => {
    const texto =  await signinTask.confirmationText()
    await expect(texto).to.equal(user)
    await signinTask.clickLogout()
})

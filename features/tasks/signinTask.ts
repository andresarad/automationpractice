import { SigninPage } from "../../page-objects/signinPage"
export class SigninTask extends SigninPage {

    clickSigninHeader() {
        this.signinHeader.waitForDisplayed({timeout : 40000})
        this.signinHeader.click()
    } 

    clickLogout(){
        this.logout.click()
    }

    public async automationpractice(email:string, password: string) {
        await this.emailInput.setValue(email)
        await this.passwordInput.setValue(password)
        await this.submitSigninBtn.click()
    }

    public async confirmationText(){
       return await this.helloUserTitle.getText();
       
   }
   
}
import {BuyProductPage} from "../../page-objects/buyProductPage"

export class BuyProductTask extends BuyProductPage {

    goToWomenShop(){
        this.categoryWomen.waitForDisplayed()
        this.categoryWomen.click()
    }

    public async selectProduct(){
        await this.selectColor.click()
        await this.productName.click()
        await this.colorBlue.click()        
        await this.availableValue.waitForDisplayed()
        await this.btnAddtoCart.waitForDisplayed({timeout: 40000 })
        await this.btnAddtoCart.click()            
    }

    public async SelectProductSoldOut(){
        await this.selectColor.click()
        await this.productName.click()
    }

    public async proccedCheck(){
        await this.proccedCheckout.waitForDisplayed()    
        await this.proccedCheckout.click();
    }

    public async productAvailable(){
        return await this.availableValue.getText()
    }

    goToCart(){
        this.shoppingCart.click()
    }

    public async searchByName(product:string){
        await this.purchaseSearch.setValue(product)
        await this.purchaseSearch.getValue()
        await this.btnSearch.click()
    }

    public async selectProductWhite(){
        await this.productName2.waitForDisplayed()
        await this.productName2.click()
        await this.colorWhite.click()        
        await this.availableValue.waitForDisplayed()
        await this.btnAddtoCart.waitForDisplayed({timeout: 40000 })
        await this.btnAddtoCart.click()            
    }    

}
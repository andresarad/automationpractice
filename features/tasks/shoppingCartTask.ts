import { ShoppingCartPage } from "../../page-objects/shoppingCartPage";

export class ShoppingCartTask extends ShoppingCartPage{

    public async buyProduct(comment: string){
        await this.proccedCheckoutShopping.click()
        await this.commentArea.setValue(comment)
        await this.proccedCheckout.click()
        await this.serviceCheckbox.click()
        await this.proccedCheckout.click()
        await this.payByCheck.click()
        await this.proccedCheckout.click()
    }

    public async confirmationPurchase() {
        await this.confirmationOrder.waitForDisplayed({timeout: 40000 })
        return await this.confirmationOrder.getText()
    }

    public async scartEmpty(){
        return await this.cartEmpty.getText()
    }

    public async buyProductBox(comment: string){
        await this.proccedCheckoutShopping.click()
        await this.commentArea.setValue(comment)
        await this.proccedCheckout.click()        
    }

    public async boxmsgError(){
        await this.proccedCheckout.click()
        await this.msgErrorBox.waitForDisplayed({timeout: 40000 })
        return await this.msgErrorBox.getText()
    }
   
}
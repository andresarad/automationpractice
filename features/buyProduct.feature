@buy-product

Feature: Make a product purchase on the "Automation Practice" page

    Scenario: Buy a product on the page
        Given The registered user opens the Automation Practice page
        When The user chooses the color and selects a short
        And The user proceeds to checkout and makes the purchase
        Then The user verifies that the order has been placed correctly

    Scenario: Purchase of an out of stock product
        Given The registered user is on the page and wants to make a purchase of an out of stock product
        When The user selects a product and tries to add the product to the cart
        Then A message is expected to be displayed indicating that the product is out of stock and not to be added to the cart

    Scenario: Making a purchase using the search functionality
        Given The registered user is on the home page of the website
        When The user enters a search term in the search field and presses enter or clicks the search button
        And The user selects the resulting product and adds it to the cart
        And The user proceeds to checkout and makes the purchase
        Then The user verifies that the order has been placed correctly

    Scenario: Purchase of multiple products
        Given The registered user is on the home page of the website
        When The user selects several products from different categories and adds them to the cart
        And The user proceeds to checkout and makes the purchase
        Then The user verifies that the order has been placed correctly

    Scenario: Check error messages when entering incorrect information
        Given The registered user has added at least one product to the shopping cart
        When The user goes to the checkout process and does not accept the terms of service
        Then  An error message is expected to be displayed


@sign-in

Feature: Log in to Automation Practice

    Scenario: A registered user logs in
        Given The user enters the Automation Practice homepage
        When The user enters the correct email and password
        Then The user sees Sign Out link on Account & Lists
